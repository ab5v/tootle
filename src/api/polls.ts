import express, { Request, Response } from "express";
import {InMemoryPollService} from "../services/InMemoryPollService";

export const pollRouter = express.Router()

const service = new InMemoryPollService()

pollRouter.get("/poll/:id", (req: Request, res: Response) => {
    console.log(req.method + " " + req.path);
    service.get(req.params.id)
        .then(poll => res.status(200).send(poll))
        .catch(err => res.status(404).send({error: err}))
});

pollRouter.post("/poll", (req: Request, res: Response) => {
    console.log(req.method + " " + req.path);
    service.create(req.body)
        .then(poll => res.status(200).send(poll))
        .catch(err => res.status(404).send({error: err}))
})


pollRouter.put("/poll/:id", (req: Request, res: Response) => {
    console.log(req.method + " " + req.path);
    service.update(req.params.id, req.body)
        .then(poll => res.status(200).send(poll))
        .catch(err => res.status(404).send({error: err}))
})

pollRouter.put("/poll/:id/vote", (req: Request, res: Response) => {
    console.log(req.method + " " + req.path);
    service.vote(req.params.id, req.body)
        .then(poll => res.status(200).send(poll))
        .catch(err => res.status(404).send({error: err}))
})
