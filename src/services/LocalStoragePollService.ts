import { v4 as uuidv4 } from 'uuid';
import { PollService, Poll, PollRequest, Voter, PollId } from "./PollService";

export class LocalStoragePollService implements PollService {
    create(request: PollRequest): Promise<Poll> {
        const poll: Poll = {
            id: uuidv4(),
            title: request.title,
            voters: [],
            options: request.options,
        }

        localStorage.setItem(poll.id, JSON.stringify(poll))

        return Promise.resolve(poll)
    }
    get(pollId: string): Promise<Poll> {
        const pollValue = localStorage.getItem(pollId);
        if (pollValue == null) {
            return Promise.reject(new Error("Poll " + pollId + " not found"))
        }
        // TODO: check
        const poll = JSON.parse(pollValue) as Poll

        return Promise.resolve(poll)
    }
    vote(pollId: PollId, voter: Voter): Promise<Poll> {
        return this.get(pollId)
            .then(poll => {
                const voters = poll.voters.filter(v => v.id !== voter.id)
                const updatedPoll: Poll = {
                    id: poll.id,
                    title: poll.title,
                    voters: voters.concat(voter),
                    options: poll.options,
                }
                localStorage.setItem(poll.id, JSON.stringify(updatedPoll))
                return updatedPoll
            });
    }

    update(pollId: PollId, pollRequest: PollRequest): Promise<Poll> {
        return this.get(pollId)
            .then(poll => {

                // TODO: extend
                const newPoll = {
                    id: poll.id,
                    title: pollRequest.title,
                    options: pollRequest.options,
                    voters: poll.voters,
                };
                localStorage.setItem(poll.id, JSON.stringify(newPoll))

                return newPoll;
            })
    }

}