export type PollId = string
export type PollOption = string
export interface Poll {
    readonly id: PollId
    readonly title: string
    readonly voters: ReadonlyArray<Voter>
    readonly options: ReadonlyArray<PollOption>
}
export interface PollRequest {
    readonly title: string
    readonly options: ReadonlyArray<PollOption>
}

export type VoterId = string
export interface Voter {
    readonly id: VoterId
    readonly name: string
    readonly votes: ReadonlyArray<PollOption>
}
export interface PollService {
    create(poll: PollRequest): Promise<Poll>
    update(pollId: PollId, poll: PollRequest): Promise<Poll>
    get(pollId: PollId): Promise<Poll>
    vote(pollId: PollId, voter: Voter): Promise<Poll>
}