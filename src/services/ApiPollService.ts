import {Poll, PollId, PollRequest, PollService, Voter} from "./PollService";
import axios from "axios";

export class ApiPollService implements PollService {

    create(request: PollRequest): Promise<Poll> {
        return axios.post("/api/poll/", request)
            .then(response => response.data);
    }

    get(pollId: PollId): Promise<Poll> {
        return axios.get("/api/poll/" + pollId)
            .then(response => response.data);
    }

    update(pollId: PollId, poll: PollRequest): Promise<Poll> {
        return axios.put("/api/poll/" + pollId, poll)
            .then(response => response.data);
    }

    vote(pollId: PollId, voter: Voter): Promise<Poll> {
        return axios.put("/api/poll/" + pollId + "/vote", voter)
            .then(response => response.data);
    }
}