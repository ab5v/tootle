import {Poll, PollId, PollRequest, PollService, Voter} from "./PollService";
import {v4 as uuidv4} from "uuid";

export class InMemoryPollService implements PollService {
    private store: Map<PollId, Poll> = new Map<PollId, Poll>()

    create(request: PollRequest): Promise<Poll> {
        console.log("Create: " + JSON.stringify(request))
        const poll: Poll = {
            id: uuidv4(),
            title: request.title,
            voters: [],
            options: request.options,
        }
        this.store.set(poll.id, poll)

        console.log("Store: " + JSON.stringify(Array.from(this.store.entries())));
        return Promise.resolve(poll)
    }

    get(pollId: PollId): Promise<Poll> {
        const poll = this.store.get(pollId);
        if (poll === undefined) {
            return Promise.reject(new Error("Poll " + pollId + " not found"))
        }
        return Promise.resolve(poll);
    }

    update(pollId: PollId, pollRequest: PollRequest): Promise<Poll> {
        return this.get(pollId)
            .then(poll => {

                // TODO: extend
                const newPoll = {
                    id: poll.id,
                    title: pollRequest.title,
                    options: pollRequest.options,
                    voters: poll.voters,
                };
                this.store.set(poll.id, newPoll)

                return newPoll;
            })
    }

    vote(pollId: PollId, voter: Voter): Promise<Poll> {
        return this.get(pollId)
            .then(poll => {
                const voters = poll.voters.filter(v => v.id !== voter.id)
                const updatedPoll: Poll = {
                    id: poll.id,
                    title: poll.title,
                    voters: voters.concat(voter),
                    options: poll.options,
                }
                this.store.set(poll.id, updatedPoll)
                return updatedPoll
            });
    }
}