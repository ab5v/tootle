import { v4 as uuidv4 } from 'uuid';
import React, { ChangeEvent, FormEvent } from "react";
import {PollOption, PollService, Voter, Poll, VoterId, PollId} from "../services/PollService";

export interface VoterState {
    id: VoterId
    name: string
    votes: ReadonlyArray<PollOption>
}
export interface VoterProps {
    pollId: PollId
    options: ReadonlyArray<PollOption>
    pollService: PollService
    pollUpdater(poll: Poll): void
}

export const defaultState: VoterState = {
    id: '',
    name: '',
    votes: [],
}

export class VoterCreateForm extends React.Component<VoterProps, VoterState> {
    state = defaultState

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        if (target.name === "name") {
            this.setState({name: target.value})
        } else if (target.name === "option") { 
            if (target.checked) {
                this.setState({votes: [target.value].concat(this.state.votes)})
            } else {
                this.setState({votes: this.state.votes.filter(item => item !== target.value)})
            }
        }
    } 

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        // TODO: service generates id for poll
        const voter: Voter = {
            id: uuidv4(),
            name: this.state.name,
            votes: this.state.votes,
        }
        this.props.pollService
            .vote(this.props.pollId, voter)
            .then(poll => {
                this.setState(defaultState)
                this.props.pollUpdater(poll)
            })
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Name:</label>
                <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                {
                    this.props.options.map(option => {
                        return (
                            <div key={option}>
                                <label> 
                                    <input
                                        type="checkbox"
                                        name="option"
                                        value={option}
                                        checked={this.state.votes.indexOf(option) > -1}
                                        onChange={this.handleChange}
                                        />
                                    {option}
                                </label>
                            </div>
                        )
                    })
                }
                <input type="submit"/>
            </form>
        )
    }
}