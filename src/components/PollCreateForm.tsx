import { navigate, RouteComponentProps } from "@reach/router";
import dayjs from 'dayjs';
import React, { ChangeEvent, FormEvent } from "react";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import { PollOption, PollService } from "../services/PollService";


export interface PollFormProps extends RouteComponentProps {
    pollService: PollService
}

export interface PollFormState {
    title: string
    selectedDays: ReadonlyArray<PollOption>
}

const birthdayStyle = `.DayPicker-Day--daySelector {
    background-color: orange;
    color: white;
  }`;

export class PollCreateForm extends React.Component<PollFormProps, PollFormState> {
    private readonly pollService: PollService

    constructor(props: PollFormProps) {
        super(props)

        this.state = {title: '', selectedDays: []}
        this.pollService = props.pollService

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleDayClick = this.handleDayClick.bind(this)
        this.isSelected = this.isSelected.bind(this)

    }
    handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
        this.pollService.create({
            title: this.state.title,
            options: this.state.selectedDays
        })
            .then(poll => navigate("p/" + poll.id))
    }
    handleChange(event: ChangeEvent<HTMLInputElement>) {
        this.setState({title: event.target.value})
    }
    handleDayClick(day: Date){
        const pollDate = dayjs(day).format("YYYY-MM-DD");
        const filtered = this.state.selectedDays.filter(item => item !== pollDate);

        if (filtered.length !== this.state.selectedDays.length) {
            this.setState({selectedDays: filtered});
        } else {
            this.setState({selectedDays: filtered.concat(pollDate).sort()})
        }
    }
    isSelected(day: Date): boolean {
        const pollDate = dayjs(day).format("YYYY-MM-DD");
        return this.state.selectedDays.indexOf(pollDate) > -1;
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <style>{birthdayStyle}</style>
                <label>Title:</label>
                <input type="text" value={this.state.title} onChange={this.handleChange} />
                <DayPicker modifiers={ {daySelector: this.isSelected }} onDayClick={this.handleDayClick}/>
            </form>
        )
    }
}