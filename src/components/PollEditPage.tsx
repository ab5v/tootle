import React from "react";
import {Poll, PollId, PollService} from "../services/PollService";
import {navigate, RouteComponentProps} from "@reach/router";
import {PollEditForm, PollEditFormProps} from "./PollEditForm";

interface PollEditProps extends RouteComponentProps<{pollId: PollId}> {
    pollService: PollService
}

interface PollEditState {
    pollId: PollId
    poll?: Poll
}

export class PollEditPage extends React.Component<PollEditProps, PollEditState>{
    private readonly pollService: PollService

    constructor(props: Readonly<PollEditProps>) {
        super(props);

        if (props.pollId === undefined) {
            throw new Error("pollId should be defined");
        }

        this.state = {pollId: props.pollId}
        this.pollService = props.pollService
    }

    handlePollUpdate = (poll: Poll): void => {
        this.setState({poll})
        navigate("/p/" + poll.id)
    }

    componentDidMount(): void {
        this.props.pollService
            .get(this.state.pollId)
            .then(poll => this.setState({poll}))
    }

    render(): React.ReactNode {
        if (this.state.poll === undefined) {
            return (<div>Loading</div>)
        }

        const editProps: PollEditFormProps = {
            poll: this.state.poll,
            pollService: this.pollService,
            handleUpdate: this.handlePollUpdate,
        }

        return (
            <PollEditForm {...editProps} />
        )

    }
}