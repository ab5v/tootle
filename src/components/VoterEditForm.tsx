import React, {ChangeEvent, FormEvent} from "react";
import {defaultState, VoterProps, VoterState} from "./VoterCreateForm";
import {Voter} from "../services/PollService";

interface VoterEditProps extends VoterProps {
    voter: Voter
}
/**
 *
 */
export class VoterEditForm extends React.Component<VoterEditProps, VoterState> {
    constructor(props: Readonly<VoterEditProps>) {
        super(props);

        this.state = props.voter
    }

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        if (target.name === "name") {
            this.setState({name: target.value})
        } else if (target.name === "option") {
            if (target.checked) {
                this.setState({votes: [target.value].concat(this.state.votes)})
            } else {
                this.setState({votes: this.state.votes.filter(item => item !== target.value)})
            }
        }
    }

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        this.props.pollService
            .vote(this.props.pollId, this.state)
            .then(poll => {
                this.setState(defaultState)
                this.props.pollUpdater(poll)
            })

    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Name:</label>
                <input type="text" name="name" value={this.state.name} onChange={this.handleChange}/>
                {
                    this.props.options.map(option => {
                        return (
                            <div key={option}>
                                <label>
                                    <input
                                        type="checkbox"
                                        name="option"
                                        value={option}
                                        checked={this.state.votes.indexOf(option) > -1}
                                        onChange={this.handleChange}
                                    />
                                    {option}
                                </label>
                            </div>
                        )
                    })
                }
                <input type="submit"/>
            </form>
        )
    }

}