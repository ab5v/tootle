import React, {ChangeEvent, FormEvent} from "react";
import {PollFormState} from "./PollCreateForm";
import DayPicker from "react-day-picker";
import {Poll, PollService} from "../services/PollService";
import dayjs from "dayjs";

const birthdayStyle = `
.DayPicker-Day--daySelector {
    background-color: orange;
    color: white;
}
`;

export interface PollEditFormProps {
    poll: Poll
    pollService: PollService
    handleUpdate(poll: Poll): void
}

export class PollEditForm extends React.Component<PollEditFormProps, PollFormState> {

    handleChange = (event: ChangeEvent<HTMLInputElement>):void => {
        this.setState({title: event.target.value})
    }

    handleDayClick = (day: Date): void => {
        const pollDate = dayjs(day).format("YYYY-MM-DD");
        const filtered = this.state.selectedDays.filter(item => item !== pollDate);

        if (filtered.length !== this.state.selectedDays.length) {
            this.setState({selectedDays: filtered});
        } else {
            this.setState({selectedDays: filtered.concat(pollDate).sort()})
        }
    }

    handleSubmit = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault()
        this.props.pollService
            .update(this.props.poll.id, {title: this.state.title, options: this.state.selectedDays})
            .then(this.props.handleUpdate)
    }
    isSelected = (day: Date): boolean => {
        const pollDate = dayjs(day).format("YYYY-MM-DD");
        return this.state.selectedDays.indexOf(pollDate) > -1;
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <style>{birthdayStyle}</style>
                <label>Title:</label>
                <input type="text" value={this.state.title} onChange={this.handleChange} />
                <DayPicker modifiers={ {daySelector: this.isSelected }} onDayClick={this.handleDayClick}/>
            </form>
        )
    }

    constructor(props: Readonly<PollEditFormProps>) {
        super(props);

        this.state = {title: props.poll.title, selectedDays: props.poll.options}
    }
}