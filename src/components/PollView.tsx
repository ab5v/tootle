import React from "react";
import { Poll, PollId, PollService } from "../services/PollService";
import {navigate, RouteComponentProps} from "@reach/router";
import { VoterCreateForm } from "./VoterCreateForm";
import {VoterEditForm} from "./VoterEditForm";

interface PollViewState {
    pollId: PollId
    poll?: Poll
    editing?: string
}
export interface PollViewProps extends RouteComponentProps<{pollId: PollId}> {
    pollService: PollService
}

export class PollView extends React.Component<PollViewProps, PollViewState> {
    private readonly pollService: PollService

    constructor(props: PollViewProps) {
        super(props)
        if (props.pollId === undefined) {
            throw new Error("Poll id is not defined")
        }

        this.state = { editing: undefined, pollId: props.pollId, poll: undefined }

        this.pollService = props.pollService
    }

    componentDidMount() {
        if (this.state.pollId) {
            this.pollService.get(this.state.pollId)
                .then(poll => this.setState({poll}))
                .catch(err => console.log(err))
        }
    }

    pollUpdate = (poll: Poll): void => {
        this.setState({poll, editing: undefined})
    }

    render() {
        const poll = this.state.poll;

        if (!poll) return <div>Loading</div>

        return (
            <div>
                <h1>{poll.title}</h1>
                <span onClick={() => navigate("/p/" + poll.id + "/edit")}>edit</span>
                <ul>
                    { poll.options.map(opt => <li key={opt}>{opt}</li>) }
                </ul>
                <h2>Voters:</h2>
                <ul>
                    {
                        poll.voters.map(voter => {
                            if (voter.id === this.state.editing) {
                                return (
                                    <li key={voter.id}>
                                        <VoterEditForm
                                            pollId={this.state.pollId}
                                            voter={voter}
                                            options={poll.options}
                                            pollService={this.props.pollService}
                                            pollUpdater={this.pollUpdate}/>
                                    </li>
                                )
                            } else {
                                return (
                                    <li key={voter.id}>
                                        {voter.name}: {voter.votes.join(', ')}
                                        <span onClick={() => this.setState({editing: voter.id})}>edit</span>
                                    </li>
                                )
                            }
                        })
                    }
                </ul>
                <VoterCreateForm pollId={poll.id} pollUpdater={this.pollUpdate} options={poll.options} pollService={this.props.pollService}/>
            </div>
        )
    }
}