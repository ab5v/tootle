import express from "express";
import {pollRouter} from "./api/polls";

const app = express();
const port = 5000;

express.Router()

export const server = app.listen(port, () => {
    console.log("App is running at http://localhost:%d in %s mode", port);
    console.log("Press CTRL-C to stop\n");

});

app.use(express.json());
app.use("/api/", pollRouter);
