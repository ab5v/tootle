import {Router} from '@reach/router';
import React from 'react';
import './App.css';
import {PollCreateForm} from './components/PollCreateForm';
import {PollView} from './components/PollView';
import {PollEditPage} from "./components/PollEditPage";
import {ApiPollService} from "./services/ApiPollService";

const pollService = new ApiPollService();

function App() {
  return (

    <Router>
      <PollCreateForm path="/" pollService={pollService}/>
      <PollEditPage path="p/:pollId/edit" pollService={pollService}/>
      <PollView path="p/:pollId" pollService={pollService}/>
    </Router>
  );
}

export default App;